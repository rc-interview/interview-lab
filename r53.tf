resource "aws_route53_zone" "private" {
  name = "sre-internal.com"

  vpc {
    vpc_id = var.vpc_id
  }
  tags = local.spt_tags
}