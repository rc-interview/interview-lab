module "tst" {
  source             = "git::ssh://git@gitlab.com:rc-interview/ec2-1disk.git?ref=v0.2.0"
  vm_count           = 1
  vm_count_offset    = 0
  host_name_prefix   = "fra20-c01-tst"
  ami_id             = var.default_ami
  subnet_ids         = ["subnet-10965566"]
  security_group_ids = [aws_security_group.allow_tst.id]
  key_pair_name      = var.key_pair_name
  tenancy            = "default"
  iam_role           = ""
  instance_type      = var.instance_type
  ebs_optimized      = "true"
  domain             = aws_route53_zone.private.name
  zone_id            = aws_route53_zone.private.zone_id

  boot_device = {
    volume_size = 10
    volume_type = "gp2"
  }

  tags = local.spt_tags
}

resource "aws_eip" "pub" {
  instance = module.tst.instances_id[0][0]
  vpc      = true
}
