terraform {
  required_version = "1.0.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.26.0"
    }
  }
  backend "s3" {
    profile                 = "sre-lab"
    shared_credentials_file = "~/.aws/credentials"
    encrypt                 = true
    bucket                  = "tfstate-remote-sre-lab"
    dynamodb_table          = "tflock-sre-lab"
    region                  = "us-west-2"
    key                     = ".terraform/ev-infrastructure/sre-lab/aws21/interview-lab/interview-lab.tfstate"
  }
}