resource "aws_security_group" "allow_tst" {
  name        = "allow_tst"
  description = "Allow tst inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    cidr_blocks = [
      "73.95.135.39/32"
    ]
    description = "Src IP"
    from_port   = 22
    protocol    = "tcp"
    self        = false
    to_port     = 22
  }

  ingress {
    cidr_blocks = [
      "73.95.135.39/32"
    ]
    description = "Src IP"
    from_port   = 80
    protocol    = "tcp"
    self        = false
    to_port     = 80
  }


  ingress {
    description = "To TST"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    self        = false
    cidr_blocks = ["10.99.0.0/24"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.spt_tags
}